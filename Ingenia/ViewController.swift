//
//  ViewController.swift
//  Ingenia
//
//  Created by Carlos Perez on 7/19/17.
//  Copyright © 2017 Carlos Perez. All rights reserved.
//

import UIKit
import AlamofireObjectMapper
import ObjectMapper
import Alamofire
import CoreData
import SwiftyJSON
import SDWebImage


class GistsResponse: Mappable {
    var url: String?
    var forks_url: String?
    var commits_url: String?
    var id: String?
    var git_pull_url: String?
    var git_push_url: String?
    var html_url: String?
    var bol_public: Bool?
    var created_at: Date?
    var updated_at: Date?
    var str_description: String?
    var comments: Int16?
    var user: String?
    var comments_url: String?
    var truncated: Bool?
    var files: Dictionary<String, FileObject> = [:]
    
    
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        url <- map["url"]
        forks_url <- map["forks_url"]
        commits_url <- map["commits_url"]
        id <- map["id"]
        git_pull_url <- map["git_pull_url"]
        git_push_url <- map["git_push_url"]
        html_url <- map["html_url"]
        bol_public <- map["public"]
        created_at <- (map["created_at"], DateTransform())
        updated_at <- (map["updated_at"], DateTransform())
        str_description <- map["description"]
        comments <- map["comments"]
        user <- map["user"]
        comments_url <- map["comments_url"]
        truncated <- map["truncated"]
        files <- map["files"]
    }
    
}

class FileObject: Mappable {
    var filename: String?
    var type: String?
    var language: String?
    var raw_url: String?
    var size: String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map) {
        filename <- map["filename"]
        type <- map["type"]
        language <- map["language"]
        raw_url <- map["raw_url"]
        size <- map["size"]
    }
}

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    


    
    @IBOutlet weak var tableView: UITableView!
    
    var rows:[Gists] = [];
    
    var theTimer: Timer!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.title = "José Carlos Pérez Quintana"
        getDataFromUrl();
        //getLocalData();
        initTableView();
        initRefreshControl();
        initTimer()
    }
    
    func initTableView() {
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(UINib(nibName: "RowTableViewCell", bundle: nil), forCellReuseIdentifier: "Cell")
    }
    
    var refreshControl: UIRefreshControl?
    
    func initRefreshControl() {
        self.refreshControl = UIRefreshControl()
        self.refreshControl?.addTarget(self, action: #selector(self.handleRefresh(refreshControl:)), for: UIControlEvents.valueChanged)
        self.tableView.addSubview(self.refreshControl!);
    }
    
    func initTimer() {
        self.theTimer = Timer.scheduledTimer(timeInterval: (5*60), target: self, selector: #selector(self.handleRefresh(refreshControl:)), userInfo: nil, repeats: true)
    }
    
    func handleRefresh(refreshControl: UIRefreshControl) {
        // Do some reloading of data and update the table view's data source
        // Fetch more objects from a web service, for example...
        
        // Simply adding an object to the data source for this example
        getDataFromUrl();
        
        //getLocalData();
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return rows.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let identifier = "Cell"
        var cell: RowTableViewCell! = tableView.dequeueReusableCell(withIdentifier: identifier) as? RowTableViewCell
        if cell == nil {
    
            cell = tableView.dequeueReusableCell(withIdentifier: identifier) as? RowTableViewCell
        }
        
        let the_gist: Gists = self.rows[indexPath.row];
        let the_files: [Files] = Array(the_gist.files!) as! [Files]
        
        if (the_files.count > 0) {
            let the_file: Files = the_files[0];
            
            cell.lblFilename.text = the_file.filename
            cell.lblDescrption.text = the_gist.str_description
            let formatter = DateFormatter()
            formatter.dateFormat = "dd/MM/yyyy HH:mm"
            cell.lblDate.text = formatter.string(from: the_gist.created_at! as Date)
            let tempNumber:NSNumber = the_gist.comments as! NSNumber
            cell.lblNumComments.text = tempNumber.stringValue
            if (the_gist.avatar_url != nil) {
                cell.imgAvatar.sd_setImage(with: URL(string: the_gist.avatar_url!))
            } else {
                setAvatarURL(the_url: the_gist.url!, the_image: cell.imgAvatar)
            }
            
            setLoginUserL(the_url: the_gist.url!, the_label: cell.lblLogin)
        }
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let the_gist: Gists = self.rows[indexPath.row];
        
        let detailVC = self.storyboard?.instantiateViewController(withIdentifier: "detail") as? DetailViewController
        detailVC?.the_gist = the_gist;
        self.navigationController?.pushViewController(detailVC!, animated: true)
    }
    
    
    func setAvatarURL(the_url: String, the_image: UIImageView) {
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer d358a476c14f3312c57f50ac958b5168a354b6d8"
        ]
        
        Alamofire.request(the_url, headers: headers).responseJSON { (response: DataResponse<Any>) in
            let jsonData = JSON(response.result.value as Any)
            let the_dict = jsonData["history"].arrayObject
            let the_history = the_dict?[0] as! NSDictionary;
            let the_user = the_history["user"] as! NSDictionary;
            let url_avatar = the_user["avatar_url"] as! String
            if  url_avatar != "" {
                the_image.sd_setImage(with: URL(string: url_avatar))
            }
        }
    }
    
    func setLoginUserL(the_url: String, the_label: UILabel) {
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer d358a476c14f3312c57f50ac958b5168a354b6d8"
        ]
        
        Alamofire.request(the_url, headers: headers).responseJSON { (response: DataResponse<Any>) in
            let jsonData = JSON(response.result.value as Any)
            let the_dict = jsonData["history"].arrayObject
            let the_history = the_dict?[0] as! NSDictionary;
            let the_user = the_history["user"] as! NSDictionary;
            let the_login = the_user["login"] as! String
            if  the_login != "" {
                the_label.text = the_login
            }
        }
    }
    
    func getAvatarURL(the_url: String, the_gist: Gists) {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = delegate.persistentContainer.viewContext
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer d358a476c14f3312c57f50ac958b5168a354b6d8"
        ]
        
        Alamofire.request(the_url, headers: headers).responseJSON { (response: DataResponse<Any>) in
            let jsonData = JSON(response.result.value as Any)
            let the_dict = jsonData["history"].arrayObject
            let the_history = the_dict?[0] as! NSDictionary;
            let the_user = the_history["user"] as! NSDictionary;
            let url_avatar = the_user["avatar_url"] as! String
            if  url_avatar != "" {
                the_gist.updateUrlAvatar(objectToUpdate: the_gist, urlAvatar: url_avatar, managedContext: managedObjectContext)
            }
        }
    }
    
    func getLoginUser(the_url: String, the_gist: Gists) {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = delegate.persistentContainer.viewContext
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer d358a476c14f3312c57f50ac958b5168a354b6d8"
        ]
        
        Alamofire.request(the_url, headers: headers).responseJSON { (response: DataResponse<Any>) in
            let jsonData = JSON(response.result.value as Any)
            let the_dict = jsonData["history"].arrayObject
            let the_history = the_dict?[0] as! NSDictionary;
            let the_user = the_history["user"] as! NSDictionary;
            let the_login = the_user["login"] as! String
            if  the_login != "" {
                the_gist.updateLoginUser(objectToUpdate: the_gist, theLogin: the_login, managedContext: managedObjectContext)
            }
        }
    }
    
    func getContentFile(the_url: String, the_gist: Gists) {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = delegate.persistentContainer.viewContext
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer d358a476c14f3312c57f50ac958b5168a354b6d8"
        ]
        
        Alamofire.request(the_url, headers: headers).responseJSON { (response: DataResponse<Any>) in
            let jsonData = JSON(response.result.value as Any)
            if let arrJSON = jsonData["files"].dictionaryObject {
                for (key, value) in arrJSON {
                    let the_dict = value as! [String: Any]
                    the_gist.updateContentFile(objectToUpdate: the_gist, contentFile: the_dict["content"] as! String, managedContext: managedObjectContext)
                }
            }
        }
    }
    
    func getDataFromUrl() {
        let gist_db: Gists = Gists();
        //gist_db.deleteAllGists();
        
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = delegate.persistentContainer.viewContext
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer d358a476c14f3312c57f50ac958b5168a354b6d8"
        ]
        
        let URL = "https://api.github.com/gists/public"
        
        Alamofire.request(URL, headers: headers).responseArray { (response: DataResponse<[GistsResponse]>) in
            print(response)
            let gistsArray = response.result.value
            
            if let gistsArray = gistsArray {
                for gist in gistsArray {
                    var dict: Dictionary<String, Any> = [:]
                    
                    dict["url"] = gist.url;
                    dict["forks_url"] = gist.forks_url;
                    dict["commits_url"] = gist.commits_url;
                    dict["id"] = gist.id;
                    dict["git_pull_url"] = gist.git_pull_url;
                    dict["git_push_url"] = gist.git_push_url;
                    dict["html_url"] = gist.html_url;
                    dict["bol_public"] = gist.bol_public;
                    dict["created_at"] = gist.created_at;
                    dict["updated_at"] = gist.updated_at;
                    dict["str_description"] = gist.str_description;
                    dict["comments"] = gist.comments;
                    dict["user"] = gist.user;
                    dict["comments_url"] = gist.comments_url;
                    dict["truncated"] = gist.truncated;
                    
                    
                    
                    let the_gist: Gists = gist_db.save(dict: dict, managedContext: managedObjectContext) as! Gists
                    
                    self.getAvatarURL(the_url: gist.url!, the_gist: the_gist)
                    self.getLoginUser(the_url: gist.url!, the_gist: the_gist)
                    self.getContentFile(the_url: gist.url!, the_gist: the_gist)
                    
                    if (gist.files.count>0) {
                        for (key, file) in gist.files {
                            print(key)
                            print(file)
                            
                            var dict2: Dictionary<String, Any> = [:]
                            
                            dict2["filename"] = file.filename;
                            dict2["type"] = file.type;
                            dict2["language"] = file.language;
                            dict2["raw_url"] = file.raw_url;
                            dict2["size"] = file.size;
                            
                            the_gist.saveFileForGist(gist: the_gist, dict: dict2, managedContext: managedObjectContext)
                        }
                    }
                }
                
                let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Gists")
                request.sortDescriptors = [NSSortDescriptor(key: "created_at", ascending: false)]
                //request.returnsObjectsAsFaults = false
                //request.predicate = NSPredicate(format: "fstrUsername = %@", pstrUsername)
                
                do {
                    self.rows = try managedObjectContext.fetch(request) as! [Gists]
                    self.reloadWithAnimAtion();
                    self.refreshControl?.endRefreshing()
                } catch let error as NSError {
                    print("Could not save. \(error), \(error.userInfo)")
                }
            }
        }
//        Alamofire.request(URL).responseJSON { (response: DataResponse<Any>) in
//            print(response)
//        }
        
        
    }
    
    func getLocalData() {
        let delegate = UIApplication.shared.delegate as! AppDelegate
        let managedObjectContext = delegate.persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Gists")
        //request.returnsObjectsAsFaults = false
        //request.predicate = NSPredicate(format: "fstrUsername = %@", pstrUsername)
        
        
        do {
            self.rows = try managedObjectContext.fetch(request) as! [Gists]
            self.reloadWithAnimAtion();
            self.refreshControl?.endRefreshing()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    func reloadWithAnimAtion() {
        let range = NSMakeRange(0, self.tableView.numberOfSections)
        let sections = NSIndexSet(indexesIn: range)
        self.tableView.reloadSections(sections as IndexSet, with: .automatic)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

