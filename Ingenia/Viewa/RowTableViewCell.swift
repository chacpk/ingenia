//
//  RowTableViewCell.swift
//  Ingenia
//
//  Created by Carlos Perez on 7/19/17.
//  Copyright © 2017 Carlos Perez. All rights reserved.
//

import UIKit

class RowTableViewCell: UITableViewCell {

    
    
    @IBOutlet weak var imgAvatar: UIImageView!
    @IBOutlet weak var lblLogin: UILabel!
    @IBOutlet weak var lblFilename: UILabel!
    @IBOutlet weak var lblDescrption: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblNumComments: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
