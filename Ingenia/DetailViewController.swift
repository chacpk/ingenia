//
//  DetailViewController.swift
//  Ingenia
//
//  Created by Carlos Perez on 7/20/17.
//  Copyright © 2017 Carlos Perez. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SDWebImage

extension Data {
    var attributedString: NSAttributedString? {
        do {
            return try NSAttributedString(data: self, options:[NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            print(error)
        }
        return nil
    }
}
extension String {
    var data: Data {
        return Data(utf8)
    }
}

class DetailViewController: UIViewController {
    
    public var the_gist: Gists?
    @IBOutlet weak var txtContent: UITextView!

    override func viewDidLoad() {
        super.viewDidLoad()
        txtContent.text = ""
        // Do any additional setup after loading the view.
        self.automaticallyAdjustsScrollViewInsets = false
        setContentFile(the_url: (the_gist?.url)!, the_label: txtContent)
    }
    
    
    func setContentFile(the_url: String, the_label: UITextView) {
        
        let headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Authorization": "bearer d358a476c14f3312c57f50ac958b5168a354b6d8"
        ]
        
        Alamofire.request(the_url, headers: headers).responseJSON { (response: DataResponse<Any>) in
            let jsonData = JSON(response.result.value as Any)
            if let arrJSON = jsonData["files"].dictionaryObject {
                for (key, value) in arrJSON {
                    let the_dict = value as! [String: Any]
                    let the_content =  the_dict["content"] as! String
                    
                    the_label.attributedText = the_content.data.attributedString
                }
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
