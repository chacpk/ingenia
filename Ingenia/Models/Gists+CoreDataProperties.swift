//
//  Gists+CoreDataProperties.swift
//  Ingenia
//
//  Created by Carlos Perez on 7/19/17.
//  Copyright © 2017 Carlos Perez. All rights reserved.
//

import Foundation
import CoreData


extension Gists {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Gists> {
        return NSFetchRequest<Gists>(entityName: "Gists")
    }

    @NSManaged public var url: String?
    @NSManaged public var login: String?
    @NSManaged public var avatar_url: String?
    @NSManaged public var content: String?
    @NSManaged public var forks_url: String?
    @NSManaged public var commits_url: String?
    @NSManaged public var identifier: String?
    @NSManaged public var git_pull_url: String?
    @NSManaged public var git_push_url: String?
    @NSManaged public var html_url: String?
    @NSManaged public var bol_public: Bool
    @NSManaged public var created_at: NSDate?
    @NSManaged public var updated_at: NSDate?
    @NSManaged public var str_description: String?
    @NSManaged public var comments: Int16
    @NSManaged public var user: String?
    @NSManaged public var comments_url: String?
    @NSManaged public var truncated: Bool
    @NSManaged public var files: NSSet?

}

// MARK: Generated accessors for files
extension Gists {

    @objc(addFilesObject:)
    @NSManaged public func addToFiles(_ value: Files)

    @objc(removeFilesObject:)
    @NSManaged public func removeFromFiles(_ value: Files)

    @objc(addFiles:)
    @NSManaged public func addToFiles(_ values: NSSet)

    @objc(removeFiles:)
    @NSManaged public func removeFromFiles(_ values: NSSet)

}
