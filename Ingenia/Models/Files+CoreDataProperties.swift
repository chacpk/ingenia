//
//  Files+CoreDataProperties.swift
//  Ingenia
//
//  Created by Carlos Perez on 7/19/17.
//  Copyright © 2017 Carlos Perez. All rights reserved.
//

import Foundation
import CoreData


extension Files {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Files> {
        return NSFetchRequest<Files>(entityName: "Files")
    }

    @NSManaged public var filename: String?
    @NSManaged public var type: String?
    @NSManaged public var language: String?
    @NSManaged public var raw_url: String?
    @NSManaged public var size: String?

}
