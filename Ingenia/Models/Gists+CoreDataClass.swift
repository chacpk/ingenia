//
//  Gists+CoreDataClass.swift
//  Ingenia
//
//  Created by Carlos Perez on 7/19/17.
//  Copyright © 2017 Carlos Perez. All rights reserved.
//

import Foundation
import CoreData

@objc(Gists)
public class Gists: NSManagedObject {
    
    public func save(dict: Dictionary<String, Any>, managedContext: NSManagedObjectContext) -> NSManagedObject {
        let entity = NSEntityDescription.entity(forEntityName: "Gists",
                                                in: managedContext)!
        
        let gist = NSManagedObject(entity: entity,
                                     insertInto: managedContext)
        
        gist.setValue(dict["bol_public"]!, forKeyPath: "bol_public")
        gist.setValue(dict["comments"]!, forKeyPath: "comments")
        gist.setValue(dict["comments_url"]!, forKeyPath: "comments_url")
        gist.setValue(dict["commits_url"]!, forKeyPath: "commits_url")
        gist.setValue(dict["created_at"]!, forKeyPath: "created_at")
        gist.setValue(dict["forks_url"]!, forKeyPath: "forks_url")
        gist.setValue(dict["git_pull_url"]!, forKeyPath: "git_pull_url")
        gist.setValue(dict["git_push_url"]!, forKeyPath: "git_push_url")
        gist.setValue(dict["html_url"]!, forKeyPath: "html_url")
        gist.setValue(dict["id"]!, forKeyPath: "identifier")
        gist.setValue((dict["str_description"] != nil) ? dict["str_description"]! : "", forKeyPath: "str_description")
        gist.setValue(dict["truncated"]!, forKeyPath: "truncated")
        gist.setValue(dict["updated_at"]!, forKeyPath: "updated_at")
        gist.setValue(dict["url"]!, forKeyPath: "url")
        gist.setValue("", forKeyPath: "user")
        
        do {
            try gist.managedObjectContext?.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
        return gist;
    }
    
    
    
    public func saveFileForGist(gist: Gists, dict: Dictionary<String, Any>, managedContext: NSManagedObjectContext) {
        
        let entity = NSEntityDescription.entity(forEntityName: "Files",
                                                in: managedContext)!
        
        let aFile:Files = NSManagedObject(entity: entity,
                                   insertInto: managedContext) as! Files
        
        // Set First and Last Name
        aFile.setValue((dict["filename"] != nil) ? dict["filename"]! : "", forKeyPath: "filename")
        aFile.setValue((dict["language"] != nil) ? dict["language"]! : "", forKeyPath: "language")
        aFile.setValue((dict["raw_url"] != nil) ? dict["raw_url"]! : "", forKeyPath: "raw_url")
        aFile.setValue((dict["size"] != nil) ? dict["size"]! : "", forKeyPath: "size")
        aFile.setValue((dict["type"] != nil) ? dict["type"]! : "", forKeyPath: "type")
        
        // Add File to Gist
        gist.addToFiles(aFile);
        
        do {
            try gist.managedObjectContext?.save()
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
        
    }
    
    public func deleteAllGists(managedContext: NSManagedObjectContext) {
        // Create Fetch Request
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Gists")
        
        // Create Batch Delete Request
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        
        do {
            try managedContext.execute(batchDeleteRequest)
            
        } catch let error as NSError {
            print("Could not save. \(error), \(error.userInfo)")
        }
    }
    
    // Gets a person by id
    func getById(id: NSManagedObjectID, managedContext: NSManagedObjectContext) -> Gists? {
        return managedContext.object(with: id) as? Gists
    }
    
    // Updates a Gists with the user URL
    public func updateUrlAvatar(objectToUpdate: Gists, urlAvatar: String, managedContext: NSManagedObjectContext){
        if let object = self.getById(id: objectToUpdate.objectID, managedContext: managedContext) {
            object.avatar_url = urlAvatar
            
            do {
                try object.managedObjectContext?.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    // Updates a Gists with the user login
    public func updateLoginUser(objectToUpdate: Gists, theLogin: String, managedContext: NSManagedObjectContext){
        if let object = self.getById(id: objectToUpdate.objectID, managedContext: managedContext) {
            object.login = theLogin
            
            do {
                try object.managedObjectContext?.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
    
    // Updates a Gists with the user with Content File
    public func updateContentFile(objectToUpdate: Gists, contentFile: String, managedContext: NSManagedObjectContext){
        if let object = self.getById(id: objectToUpdate.objectID, managedContext: managedContext) {
            object.content = contentFile
            
            do {
                try object.managedObjectContext?.save()
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
            }
        }
    }
}
